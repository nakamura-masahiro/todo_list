<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>タスク新規登録</title>
	</head>
	<body>
		<div class="main-contents">

			<div class="signup">タスク新規登録</div>

			<div class="taskForm">
				<form:form modelAttribute="taskForm">
					<table>
						<tr>
							<td><form:label path="task">タスク：</form:label></td>
							<td><form:input path="task"/></td>
						</tr>
						<tr>
							<td><form:label path="timeLimit">期限：</form:label></td>
							<td><form:input type="date" path="timeLimit"/></td>
						</tr>
					</table>
					<form:input type="hidden" path="conditionId" value="1"/>
					<input type="submit" value="登録"/>
				</form:form>
			</div>
		</div>
	</body>
</html>