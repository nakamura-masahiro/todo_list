package jp.co.todolist.service;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.todolist.dto.TaskConditionDto;
import jp.co.todolist.dto.TaskDto;
import jp.co.todolist.entity.Task;
import jp.co.todolist.entity.TaskCondition;
import jp.co.todolist.form.TaskForm;
import jp.co.todolist.form.UpdateTaskForm;
import jp.co.todolist.mapper.TaskConditionMapper;
import jp.co.todolist.mapper.TaskMapper;

@Service
public class TaskService {

	@Autowired
	private TaskConditionMapper taskConditionMapper;

	@Autowired
	private TaskMapper taskMapper;

	public List<TaskConditionDto> getTaskConditionAll(){
		List<TaskCondition> taskConditionList = taskConditionMapper.getTaskConditionAll();
	    List<TaskConditionDto> resultList = convertToDto(taskConditionList);
	    return resultList;
	}

	private List<TaskConditionDto> convertToDto(List<TaskCondition> taskConditionList) {
	    List<TaskConditionDto> resultList = new LinkedList<TaskConditionDto>();
	    for (TaskCondition entity : taskConditionList) {
	    	TaskConditionDto dto = new TaskConditionDto();
	        BeanUtils.copyProperties(entity, dto);
	        resultList.add(dto);
	    }
	    return resultList;
	}

	public void insertTask(TaskForm taskForm) {
		TaskDto taskDto = new TaskDto();
		BeanUtils.copyProperties(taskForm, taskDto);
		taskMapper.insertTask(taskDto);
	}

	public void deleteTask(int id) {
		taskMapper.deleteTask(id);
	}

	//オートボクシング機能により自動的にInteger型からint型に変換
	public UpdateTaskForm getTask(int id) {
		Task entity = taskMapper.getTask(id);
		TaskDto dto = new TaskDto();
		BeanUtils.copyProperties(entity, dto);

		//entityのDATE型のtimeLimitをString型に変換して、dtoのString型のtimeLimitにセット
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		dto.setTimeLimit(sdf.format(entity.getTimeLimit()));

		UpdateTaskForm form = new UpdateTaskForm();
		BeanUtils.copyProperties(dto, form);
		return form;
	}

	//オートボクシング機能により自動的にInteger型からint型に変換
	public Task existTaskId(int id) {
		Task entity = taskMapper.existTaskId(id);
		return entity;
	}

	public void updateTask(UpdateTaskForm updateTaskForm) {
		TaskDto updateTaskDto = new TaskDto();
		BeanUtils.copyProperties(updateTaskForm, updateTaskDto);
		taskMapper.updateTask(updateTaskDto);
	}
}
