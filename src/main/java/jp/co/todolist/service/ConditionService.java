package jp.co.todolist.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.todolist.dto.ConditionDto;
import jp.co.todolist.entity.Condition;
import jp.co.todolist.mapper.ConditionMapper;

@Service
public class ConditionService {

	@Autowired
	private ConditionMapper conditionMapper;

	public List<ConditionDto> getConditionAll(){
		List<Condition> conditionList = conditionMapper.getConditionAll();
	    List<ConditionDto> resultList = convertToDto(conditionList);
	    return resultList;
	}

	private List<ConditionDto> convertToDto(List<Condition> conditionList) {
	    List<ConditionDto> resultList = new LinkedList<ConditionDto>();
	    for (Condition entity : conditionList) {
	    	ConditionDto dto = new ConditionDto();
	        BeanUtils.copyProperties(entity, dto);
	        resultList.add(dto);
	    }
	    return resultList;
	}
}
