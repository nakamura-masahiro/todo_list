package jp.co.todolist.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.todolist.dto.TaskConditionDto;
import jp.co.todolist.form.TaskForm;
import jp.co.todolist.service.TaskService;

@Controller
public class SignupController {

	@Autowired
	private TaskService taskService;

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String insertTask(@Valid @ModelAttribute TaskForm taskForm, BindingResult result, Model model) {
		if(result.hasErrors()) {

			List<TaskConditionDto> taskConditions = taskService.getTaskConditionAll();
	        model.addAttribute("taskConditions", taskConditions);

	        TaskForm deleteTaskForm = new TaskForm();
	        model.addAttribute("deleteTaskForm", deleteTaskForm);
			return "home";
		}

		taskService.insertTask(taskForm);
		return "redirect:/";
	}
}
