package jp.co.todolist.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.todolist.dto.ConditionDto;
import jp.co.todolist.dto.TaskConditionDto;
import jp.co.todolist.entity.Task;
import jp.co.todolist.form.TaskForm;
import jp.co.todolist.form.UpdateTaskForm;
import jp.co.todolist.service.ConditionService;
import jp.co.todolist.service.TaskService;

@Controller
public class UpdateController {

	@Autowired
	private TaskService taskService;

	@Autowired
	private ConditionService conditionService;

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String getUpdate(Model model, @RequestParam(value = "taskId", defaultValue = "") String taskId,
			RedirectAttributes redirectAttributes) {

		Integer parseTaskId = checkTaskIdParam(taskId);

		if (parseTaskId == null) {
			redirectAttributes.addFlashAttribute("errorMessage", "不正なパラメータが入力されました");
			return "redirect:/";
		}

		UpdateTaskForm updateTaskForm = taskService.getTask(parseTaskId);
		model.addAttribute("updateTaskForm", updateTaskForm);
		List<ConditionDto> conditions = conditionService.getConditionAll();
		model.addAttribute("conditions", conditions);

		TaskForm taskForm = new TaskForm();//追加
        model.addAttribute("taskForm", taskForm);//追加

		List<TaskConditionDto> taskConditions = taskService.getTaskConditionAll();//追加
        model.addAttribute("taskConditions", taskConditions);//追加

        TaskForm deleteTaskForm = new TaskForm();//追加
        model.addAttribute("deleteTaskForm", deleteTaskForm);//追加
		return "home";//updateから変更
	}

	private Integer checkTaskIdParam(String taskId) {

		if (! taskId.matches("^[0-9]*$") || StringUtils.isEmpty(taskId)) {
			return null;
		}

		Integer parseTaskId = Integer.valueOf(taskId);
		Task existTask = taskService.existTaskId(parseTaskId);

		if (existTask == null) {
			return null;
		}

		return parseTaskId;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateTask(@Valid @ModelAttribute UpdateTaskForm updateTaskForm, BindingResult result, Model model) {

		if(result.hasErrors()) {
			List<ConditionDto> conditions = conditionService.getConditionAll();//追加
			model.addAttribute("conditions", conditions);//追加

			TaskForm taskForm = new TaskForm();//追加
	        model.addAttribute("taskForm", taskForm);//追加

			List<TaskConditionDto> taskConditions = taskService.getTaskConditionAll();//追加
	        model.addAttribute("taskConditions", taskConditions);//追加

	        TaskForm deleteTaskForm = new TaskForm();//追加
	        model.addAttribute("deleteTaskForm", deleteTaskForm);//追加
			return "home";//追加
		}

		taskService.updateTask(updateTaskForm);
		return "redirect:/";
	}
}