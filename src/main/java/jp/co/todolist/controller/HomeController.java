package jp.co.todolist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.todolist.dto.TaskConditionDto;
import jp.co.todolist.form.TaskForm;
import jp.co.todolist.service.TaskService;

@Controller
public class HomeController {

	@Autowired
	private TaskService taskService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
    public String getTaskConditionAll(Model model) {
		TaskForm taskForm = new TaskForm();
        model.addAttribute("taskForm", taskForm);

		List<TaskConditionDto> taskConditions = taskService.getTaskConditionAll();
        model.addAttribute("taskConditions", taskConditions);

        TaskForm deleteTaskForm = new TaskForm();
        model.addAttribute("deleteTaskForm", deleteTaskForm);
        return "home";
    }

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String deleteTask(@ModelAttribute TaskForm deleteTaskForm, Model model) {
		taskService.deleteTask(deleteTaskForm.getId());
		return "redirect:/";
	}
}
