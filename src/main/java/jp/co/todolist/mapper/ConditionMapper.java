package jp.co.todolist.mapper;

import java.util.List;

import jp.co.todolist.entity.Condition;

public interface ConditionMapper {

	List<Condition> getConditionAll();

}
