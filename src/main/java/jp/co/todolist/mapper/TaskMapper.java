package jp.co.todolist.mapper;

import jp.co.todolist.dto.TaskDto;
import jp.co.todolist.entity.Task;

public interface TaskMapper {

	void insertTask(TaskDto taskDto);
	void deleteTask(int id);
	Task getTask(int id);
	Task existTaskId(int id);
	void updateTask(TaskDto updateTaskDto);

}
