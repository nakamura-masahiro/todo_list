package jp.co.todolist.mapper;

import java.util.List;

import jp.co.todolist.entity.TaskCondition;

public interface TaskConditionMapper {

	List<TaskCondition> getTaskConditionAll();

}
