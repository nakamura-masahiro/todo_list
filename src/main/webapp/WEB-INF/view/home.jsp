<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ホーム</title>
		<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>
	</head>
	<body>
		<div class="main-contents">
			<h1>オレのToDoリスト</h1>

			<c:if test="${not empty errorMessage}">
				<div class="errorMessages">
					<c:out value="${errorMessage}"></c:out>
				</div>
			</c:if>

			<div class="taskForm">
				<form:form modelAttribute="taskForm" action="./signup">
					<table>
						<tr>
							<td><form:label path="task">タスク：</form:label></td>
							<td>
								<form:input path="task"/>
								<span class="errorMessages"><form:errors path="task"/></span>
							</td>
						</tr>
						<tr>
							<td><form:label path="timeLimit">期限：</form:label></td>
							<td>
								<form:input type="date" path="timeLimit"/>
								<span class="errorMessages"><form:errors path="timeLimit"/></span>
							</td>
						</tr>
					</table>
					<form:input type="hidden" path="conditionId" value="2"/>
					<input type="submit" value="登録" class="submitButton"/>
				</form:form>
			</div>

			<div class="list">
				<c:if test="${empty taskConditions}">
					<p>タスクを登録して下さい</p>
				</c:if>

				<c:if test="${not empty taskConditions}">
					<table border="1">
						<tr>
							<th>状態</th>
							<th>タスク</th>
							<th>期限</th>
							<th>編集</th>
							<th>削除</th>
						</tr>

						<c:forEach var="taskCondition" items="${taskConditions}">
							<tr>
								<c:if test="${empty updateTaskForm}">
									<td><c:out value="${taskCondition.status}"></c:out></td>
									<td><c:out value="${taskCondition.task}"></c:out></td>
									<td><fmt:formatDate value="${taskCondition.timeLimit}" pattern="yyyy/MM/dd" /></td>
									<td>
										<form action="./update" method="GET">
											<input type="hidden" name="taskId" value="${taskCondition.id}"/>
											<button type="submit">編集</button>
										</form>
									</td>
									<td>
										<form:form modelAttribute="deleteTaskForm" action="./">
											<form:input type="hidden" path="id" value="${taskCondition.id}"/>
											<button type="submit" class="deleteButton">削除</button>
										</form:form>
									</td>
								</c:if>

								<c:if test="${(not empty updateTaskForm) && (taskCondition.id == updateTaskForm.id)}">
									<form:form modelAttribute="updateTaskForm">
										<td>
											<form:select path="conditionId" items="${conditions}" itemValue ="id" itemLabel="status"/>
										</td>
										<td>
											<form:input path="task"/>
											<div class="errorMessages"><form:errors path="task"/></div>
										</td>
										<td>
											<form:input type="date" path="timeLimit"/>
											<div class="errorMessages"><form:errors path="timeLimit"/></div>
										</td>
										<td>
											<form:input type="hidden" path="id"/>
											<button type="submit" id="updateButton">更新</button>
										</td>
									</form:form>
										<td>
											<form:form modelAttribute="deleteTaskForm" action="./">
												<form:input type="hidden" path="id" value="${taskCondition.id}"/>
												<button type="submit" class="deleteButton">削除</button>
											</form:form>
										</td>
								</c:if>

								<c:if test="${(not empty updateTaskForm) && (taskCondition.id != updateTaskForm.id)}">
									<td><c:out value="${taskCondition.status}"></c:out></td>
									<td><c:out value="${taskCondition.task}"></c:out></td>
									<td><fmt:formatDate value="${taskCondition.timeLimit}" pattern="yyyy/MM/dd" /></td>
									<td>
										<form action="./update" method="GET">
											<input type="hidden" name="taskId" value="${taskCondition.id}"/>
											<button type="submit">編集</button>
										</form>
									</td>
									<td>
										<form:form modelAttribute="deleteTaskForm" action="./">
											<form:input type="hidden" path="id" value="${taskCondition.id}"/>
											<button type="submit" class="deleteButton">削除</button>
										</form:form>
									</td>
								</c:if>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</div>
		</div>
	</body>
</html>