<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>タスク編集</title>
	</head>
	<body>
		<div class="main-contents">

			<h1>タスク編集</h1>

			<div class="updateTaskForm">
				<form:form modelAttribute="updateTaskForm">
					<table>
						<tr>
							<td><form:label path="conditionId">状態：</form:label></td>
						  	<td><form:select path="conditionId" items="${conditions}" itemValue ="id" itemLabel="status"/></td>
		                </tr>
						<tr>
							<td><form:label path="task">タスク：</form:label></td>
							<td><form:input path="task"/></td>
						</tr>
						<tr>
							<td><form:label path="timeLimit">期限：</form:label></td>
							<td><form:input type="date" path="timeLimit"/></td>
						</tr>
					</table>
					<form:input type="hidden" path="id"/>
					<input type="submit" value="更新"/>
				</form:form>
			</div>
		</div>
	</body>
</html>